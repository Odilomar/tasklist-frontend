export interface TaskInterface{
    id: number;
    title: string;
    description: string;
    status: number;
}

export class TaskModel implements TaskInterface{
    id: number;
    title: string;
    description: string;
    status: number;

    constructor(
        id: number = 0,
        title: string = "",
        description: string = "",
        status: number = 0
    ){
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
    }

}