import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { TaskInterface } from '../../model/task.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  public getAllTasks(): Observable<TaskInterface[]>{
    return this.http.get<TaskInterface[]>(`${environment.urlApi}/task`);
  }

  public createTask(task: TaskInterface){
    return this.http.post(`${environment.urlApi}/task`, task, httpOptions);
  }

  public updateTask(task: TaskInterface): Observable<TaskInterface>{
    return this.http.put<TaskInterface>(`${environment.urlApi}/task/${task.id}`, task, httpOptions);
  }

  public deleteTask(task: TaskInterface): Observable<{}>{
    return this.http.delete(`${environment.urlApi}/task/${task.id}`);
  }
}
