import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { HttpService } from './core/services/http/http.service';
import { Subscription } from 'rxjs';
import { TaskModel, TaskInterface } from './core/model/task.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  // public todo = ['Get to work', 'Pick up groceries', 'Go home', 'Fall asleep'];
  // public doing = ['coisa', 'coisinha'];
  // public done = [
  //   'Get up',
  //   'Brush teeth',
  //   'Take a shower',
  //   'Check e-mail',
  //   'Walk dog',
  // ];

  public todo: TaskInterface[];
  public doing: TaskInterface[];
  public done: TaskInterface[];

  public _createEditTask: TaskInterface;

  public todoLength: number;
  public doingLength: number;
  public doneLength: number;

  public faPlus;

  private subscription: Subscription;

  constructor(
    private httpService: HttpService,
    private toastrService: ToastrService
    ) {
    this.todoLength = 0;
    this.doingLength = 0;
    this.doneLength = 0;

    this.todo = [];
    this.doing = [];
    this.done = [];

    this.faPlus = faPlus;
    this.subscription = new Subscription();
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.searchTasks();
  }

  public searchTasks() {
    this.subscription = this.httpService
      .getAllTasks()
      .subscribe((response: TaskInterface[]) => {
        this.todo = [];
        this.doing = [];
        this.done = [];

        response.forEach((value) => {
          if (value.status == 0) this.todo.push(value);
          if (value.status == 1) this.doing.push(value);
          if (value.status == 2) this.done.push(value);
        });
      });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }

  public setArrayLength() {
    this.todoLength = this.todo.length;
    this.doingLength = this.doing.length;
    this.doneLength = this.done.length;
  }

  public drop(event: CdkDragDrop<TaskInterface[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      event.previousContainer.data[event.previousIndex].status =
        event.container.data == this.todo
          ? 0
          : event.container.data == this.doing
          ? 1
          : 2;
      let taskTemp: TaskInterface =
        event.previousContainer.data[event.previousIndex];

      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      this.httpService
        .updateTask(taskTemp)
        .subscribe((response) => this.toastrService.success("Task atualizada com sucesso!"));
    }

    this.setArrayLength();
  }

  public createEditTask(id: number = 0, whichArray: number = 0) {
    if (id == 0) this._createEditTask = new TaskModel();
    if (id > 0 && whichArray == 1)
      this.todo.forEach((value) => {
        if (value.id == id) this._createEditTask = value;
      });
    if (id > 0 && whichArray == 2)
      this.doing.forEach((value) => {
        if (value.id == id) this._createEditTask = value;
      });
    if (id > 0 && whichArray == 3)
      this.done.forEach((value) => {
        if (value.id == id) this._createEditTask = value;
      });
  }

  public reloadTasks(reload: string) {
    if (reload) this.searchTasks();
  }
}
