import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { TaskInterface } from '../core/model/task.model';
import { HttpService } from '../core/services/http/http.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-edit-modal',
  templateUrl: './create-edit-modal.component.html',
  styleUrls: ['./create-edit-modal.component.css'],
})
export class CreateEditModalComponent implements OnInit, OnChanges {
  @Input() task: TaskInterface;
  @Output() reload: EventEmitter<boolean> = new EventEmitter<boolean>();

  public modalTitle: string;

  constructor(
    private httpService: HttpService,
    private toastrService: ToastrService
  ) {}

  ngOnChanges(): void {
    this.fillModal();
  }

  ngOnInit(): void {
    this.fillModal();
  }

  public fillModal() {
    this.modalTitle = this.task.id == 0 ? 'Create Task' : 'Edit Task';
  }

  public createEditTask() {
    if (this.task.id == 0)
      this.httpService
        .createTask(this.task)
        .subscribe((response) => {this.toastrService.success("Task cadastrada com sucesso!"); this.reload.emit(true);});
    if (this.task.id > 0)
      this.httpService
        .updateTask(this.task)
        .subscribe((response) => {this.toastrService.success("Task atualizada com sucesso!"); this.reload.emit(true);});

    this.closeModal();
  }

  public deleteTask() {
    if (this.task.id > 0)
      this.httpService
        .deleteTask(this.task)
        .subscribe((response) => {this.toastrService.success("Task removida com sucesso!"); this.reload.emit(true);});
    this.closeModal();
  }

  private closeModal() {
    let element: HTMLElement = document.getElementsByClassName(
      'close'
    )[0] as HTMLElement;
    element.click();
  }
}
